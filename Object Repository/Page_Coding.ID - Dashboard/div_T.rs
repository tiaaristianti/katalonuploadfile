<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_T</name>
   <tag></tag>
   <elementGuidId>7f0d61cf-f903-4c01-9721-2e7ee9a8a374</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.rounded-circle.author-box-picture</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>48a1c92d-6a8d-4928-939d-7349b5aa3afa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rounded-circle author-box-picture</value>
      <webElementGuid>fec06b0a-1b86-4580-9a49-95e85bab6082</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                        T
                                </value>
      <webElementGuid>6f2e8742-4006-4a5b-8cee-c49d39d90901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row mt-4 align-items-end&quot;]/div[@class=&quot;col-lg-3 col-md-4 col-sm-12&quot;]/div[@class=&quot;rounded-circle author-box-picture&quot;]</value>
      <webElementGuid>1a4f63f9-4a94-44b5-a921-d7fa830c83ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div</value>
      <webElementGuid>7f00fd4b-4fb0-4843-a378-d0207637e077</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/following::div[3]</value>
      <webElementGuid>9bf367e6-b81d-4b35-b50a-761c7fef7020</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[9]</value>
      <webElementGuid>209ef633-94db-478e-a63a-f95101ad17e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tia Aristianti'])[1]/preceding::div[1]</value>
      <webElementGuid>8316cae7-7b19-4b0c-a2e5-87acdbce6eca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div/div/div/div</value>
      <webElementGuid>98ffbe3b-7134-4686-bf62-7a2a7facb625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                    
                                        T
                                ' or . = '
                                    
                                        T
                                ')]</value>
      <webElementGuid>6abde199-c229-4a72-b16b-d8889874ce83</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
